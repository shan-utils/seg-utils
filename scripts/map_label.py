#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Map segmentation labels')
parser.add_argument('-i', '--label-image', required=True,
                    help='Image to map labels from')
parser.add_argument('-o', '--mapped-image', required=True,
                    help='Image to map labels to')
help = ('Table of mapping info. This is a .json file with the labels of the '
        'original label image as keys and labels of the converted image as '
        'values. For example, {12: 100, 13:100, 14:101} will map 12 and 13 to '
        '100 and 14 to 101.')
parser.add_argument('-l', '--label-table', required=True, help=help)
args = parser.parse_args()

import json
import nibabel as nib
import numpy as np

with open(args.label_table) as label_table_file:
    label_table = json.load(label_table_file)
input_obj = nib.load(args.label_image)
input_image = input_obj.get_data()
output_image = np.copy(input_image)
for source_label, target_label in label_table.items():
    mask = input_image == int(source_label)
    output_image[mask] = int(target_label)
output_obj = nib.Nifti1Image(output_image, input_obj.affine, input_obj.header)
output_obj.to_filename(args.mapped_image)
