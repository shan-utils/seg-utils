#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Map segmentation labels')
parser.add_argument('label_image', help='Image to list labels from')
args = parser.parse_args()

import nibabel as nib
import numpy as np

data = nib.load(args.label_image).get_data()
labels = np.unique(data).astype(int)
for l in labels:
    print(l)
